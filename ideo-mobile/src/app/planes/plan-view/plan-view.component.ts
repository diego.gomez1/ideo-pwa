import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Plan } from 'src/app/models/plan.model';
import { DirectusService } from 'src/app/directus.service';

@Component({
  selector: 'app-plan-view',
  templateUrl: './plan-view.component.html',
  styleUrls: ['./plan-view.component.scss']
})
export class PlanViewComponent implements OnInit {

  plan$: Observable<Plan>;

  constructor(private route: ActivatedRoute, private directus: DirectusService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: any) => {
      let id = params.params.id;
      console.log(params);
      this.plan$ = this.directus.getPlan(id);
    })
  }

}
