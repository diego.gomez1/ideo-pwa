import { Component, OnInit } from '@angular/core';
import { DirectusService } from '../directus.service';
import { Observable } from 'rxjs';
import { Plan } from '../models/plan.model';

@Component({
  selector: 'app-planes',
  templateUrl: './planes.component.html',
  styleUrls: ['./planes.component.scss']
})
export class PlanesComponent implements OnInit {
  planes$: Observable<Plan[]>;

  constructor(private directus: DirectusService) { }

  ngOnInit() {
    this.planes$ = this.directus.getPlanes();
  }

  getThumbnail(plan: Plan) {
    if(plan.cabecera) {
      let cabecera = plan.cabecera;
      if (cabecera.data.thumbnails.length > 0) {
        let thumbnail = cabecera.data.thumbnails[1];
        return thumbnail.url;
      }
    } else {
      return "";
    }
  }

}
