import { Component, OnInit } from '@angular/core';
import { Repercusion } from '../models/repercusion.model';
import { Observable } from 'rxjs';
import { DirectusService } from '../directus.service';

@Component({
  selector: 'app-medios',
  templateUrl: './medios.component.html',
  styleUrls: ['./medios.component.scss']
})
export class MediosComponent implements OnInit {

  repercusiones$: Observable<Repercusion[]>;

  constructor(private directus: DirectusService) { }

  ngOnInit() {
    this.repercusiones$ = this.directus.getRepercusiones();
  }


}
