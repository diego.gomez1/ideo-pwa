import { Component, OnInit } from '@angular/core';
import { Principio } from '../models/principio.model';
import { Observable } from 'rxjs';
import { DirectusService } from '../directus.service';

@Component({
  selector: 'app-principios',
  templateUrl: './principios.component.html',
  styleUrls: ['./principios.component.scss']
})
export class PrincipiosComponent implements OnInit {

  principios$: Observable<Principio[]>;

  constructor(private directus: DirectusService) { }

  ngOnInit() {
    this.principios$ = this.directus.getPrincipios();
  }

}
