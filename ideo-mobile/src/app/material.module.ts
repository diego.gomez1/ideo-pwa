import { NgModule } from '@angular/core';
import {
  MatSidenavModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatButtonModule,
  MatRippleModule,
  MatCardModule,
  MatExpansionModule
} from '@angular/material';
import {
  FlexLayoutModule
} from '@angular/flex-layout'


@NgModule({
  declarations: [],
  imports: [
    MatCardModule,
    MatExpansionModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
    MatListModule,
    FlexLayoutModule,
  ],
  exports: [
    MatCardModule,
    MatExpansionModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
    MatListModule,
    FlexLayoutModule,
  ]

})
export class MaterialModule { }
