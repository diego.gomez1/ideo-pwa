import { Deserializable } from './deserializable.model';
import { Picture } from './picture.model';

export class Plan implements Deserializable{
    id: number;
    sort: number;
    nombre: string;
    resumen: string;
    detalle: string;
    cabecera: Picture;

    deserialize(input: any) {
        Object.assign(this, input);
        if (this.cabecera) {
            this.cabecera = new Picture().deserialize(this.cabecera);
        }
        return this;
    }
}