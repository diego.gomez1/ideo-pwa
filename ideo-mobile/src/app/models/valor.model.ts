import { Deserializable } from './deserializable.model';
import { Picture } from './picture.model';

export class Valor implements Deserializable{
    id: number;
    sort: number;
    nombre: string;
    resumen: string;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}