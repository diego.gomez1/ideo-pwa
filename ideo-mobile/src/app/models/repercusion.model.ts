import { Deserializable } from './deserializable.model';
import { Picture } from './picture.model';

export class Repercusion implements Deserializable{
    id: number;
    titulo: string;
    medio: string;
    resumen: string;
    enlace: string;
    imagen: Picture;
    fecha_publicacion: Date;

    deserialize(input: any) {
        Object.assign(this, input);
        if (this.imagen) {
            this.imagen = new Picture().deserialize(this.imagen);
        }
        return this;
    }
}