import { Picture } from './picture.model';
import { Deserializable } from './deserializable.model';

export class Principal implements Deserializable {
    foto_cabecera: Picture;
    created_on: Date;

    deserialize(input: any) {
        Object.assign(this, input);
        this.foto_cabecera = new Picture().deserialize(input.foto_cabecera);
        return this;
    }
}