import { Deserializable } from './deserializable.model';

export class Thumbnail implements Deserializable {
    url: String;
    relativeUrl: String;
    dimension: String;
    width: Number;
    height: Number;

    deserialize(input: any) {
        return this;
    }
}

export class ImageData implements Deserializable {
    full_url: String;
    url: String;
    thumbnails: Array<Thumbnail>;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}

export class Picture implements Deserializable {
    id: Number;
    filename: String;
    type: String;
    width?: Number;
    height?: Number;
    data?: ImageData;

    deserialize(input: any) {
        Object.assign(this, input);
        this.data = new ImageData().deserialize(input.data);
        return this;
    }
}