import { Deserializable } from './deserializable.model';

export class Principio implements Deserializable{
    id: number;
    sort: number;
    nombre: string;
    resumen: string;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}