import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PlanesComponent } from './planes/planes.component';
import { PlanViewComponent } from './planes/plan-view/plan-view.component';
import { ValoresComponent } from './valores/valores.component';
import { PrincipiosComponent } from './principios/principios.component';
import { MediosComponent } from './medios/medios.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent, data: {animation: 'HomePage'}},
  { path: 'planes', component: PlanesComponent, data: {animation: 'PlanesPage'}},
  { path: 'valores', component: ValoresComponent, data: {animation: 'PlanesPage'}},
  { path: 'principios', component: PrincipiosComponent, data: {animation: 'PlanesPage'}},
  { path: 'repercusiones', component: MediosComponent, data: {animation: 'PlanesPage'}},
  { path: 'planes/:id', component: PlanViewComponent, data: {animation: 'PlanViewPage'}},
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
