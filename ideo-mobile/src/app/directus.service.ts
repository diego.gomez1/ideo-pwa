import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Principal } from './models/principal.model';
import { Plan } from './models/plan.model';
import { Valor } from './models/valor.model';
import { Repercusion } from './models/repercusion.model';

@Injectable({
  providedIn: 'root'
})
export class DirectusService {

  constructor(private http: HttpClient) { }

  getHome() : Observable<Principal> {
    const queryUrl = 'https://dapi.escuelaideo.edu.es/_/items/principal?single=1&fields=*.*';

    return this.http.get(queryUrl).pipe(      
      map((res:any) => {
        return new Principal().deserialize(res.data);
      })
    )
  }

  getPlanes() : Observable<Plan[]> {
    const queryUrl = 'https://dapi.escuelaideo.edu.es/_/items/planes?fields=id,sort,nombre,resumen,detalle,cabecera.data.*';

    return this.http.get(queryUrl).pipe(      
      map((res:any) => {
        let planes : any[] = res.data;
        return planes.map(plan => new Plan().deserialize(plan));
      })
    )
  }

  getPlan(id: number) : Observable<Plan> {
    /*
    const queryUrl = `https://dapi.escuelaideo.edu.es/_/items/planes/${id}?fields=id,sort,nombre,resumen,detalle,cabecera.data.*`;

    return this.http.get(queryUrl).pipe(      
      map((res:any) => {
        let plan : any = res.data;
        return new Plan().deserialize(plan);
      })
    ) */

    return this.getPlanes().pipe(
      map((planes: Plan[]) => {
        let matchPlan = null;

        planes.map((plan:Plan) => {
          if (plan.id == id) {
            matchPlan = plan;
          }
        });

        return matchPlan;
      })
    )
  }

  getValores() : Observable<Valor[]> {
    const queryUrl = 'https://dapi.escuelaideo.edu.es/_/items/valores?fields=id,nombre,resumen';

    return this.http.get(queryUrl).pipe(      
      map((res:any) => {
        let valores : any[] = res.data;
        return valores.map(valor => new Valor().deserialize(valor));
      })
    )
  }

  getPrincipios() : Observable<Valor[]> {
    const queryUrl = 'https://dapi.escuelaideo.edu.es/_/items/principios?fields=id,nombre,resumen';

    return this.http.get(queryUrl).pipe(      
      map((res:any) => {
        let valores : any[] = res.data;
        return valores.map(valor => new Valor().deserialize(valor));
      })
    )
  }

  getRepercusiones() : Observable<Repercusion[]> {
    const queryUrl = 'https://dapi.escuelaideo.edu.es/_/items/repercusion?fields=id,titulo,medio,imagen.data,enlace,resumen,fecha_publicacion&sort=-fecha_publicacion';

    return this.http.get(queryUrl).pipe(      
      map((res:any) => {
        let repercusiones : any[] = res.data;
        return repercusiones.map(repercusion => new Repercusion().deserialize(repercusion));
      })
    )
  }
}
