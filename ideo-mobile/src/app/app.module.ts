import { MaterialModule } from './material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { PlanesComponent } from './planes/planes.component';
import { ValoresComponent } from './valores/valores.component';
import { PrincipiosComponent } from './principios/principios.component';
import { MediosComponent } from './medios/medios.component';
import { PlanViewComponent } from './planes/plan-view/plan-view.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlanesComponent,
    ValoresComponent,
    PrincipiosComponent,
    MediosComponent,
    PlanViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
