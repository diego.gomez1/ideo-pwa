import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Principal } from '../models/principal.model';
import { DirectusService } from '../directus.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  principal$: Observable<Principal>;
  @Input() leftPanel = false;

  constructor(private directus: DirectusService) { }

  ngOnInit() {
    this.principal$ = this.directus.getHome();
  }

}
