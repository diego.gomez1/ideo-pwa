import { Component, OnInit } from '@angular/core';
import { Valor } from '../models/valor.model';
import { Observable } from 'rxjs';
import { DirectusService } from '../directus.service';

@Component({
  selector: 'app-valores',
  templateUrl: './valores.component.html',
  styleUrls: ['./valores.component.scss']
})
export class ValoresComponent implements OnInit {

  valores$: Observable<Valor[]>;

  constructor(private directus: DirectusService) { }

  ngOnInit() {
    this.valores$ = this.directus.getValores();
  }

}
