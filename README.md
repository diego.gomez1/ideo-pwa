# Configuración de proyecto IDEO PWA

## Configuración de directus

Directus es la API que proporciona la gestión de contenidos que se mostrarán en la web PWA de IDEO.

### Arranque de la instancia

Todo el sistema está construido sobre varios nodos de Docker.

``` 
docker-compose up -d
```

### Arranque de instancia de desarrollo

```
docker-compose -f docker-compose-dev.yml up -d
```


### Iniciar usuario de prueba

```
docker-compose run api install --email your@email.com --password somepass
```

Alternativamente se puede hacer con los siguientes comandos en la máquina de api

```bash
cd /var/www
php bin/directus install:database -f
php bin/directus install:install -e diego.gomez@escuelaideo.edu.es -p 1Ns3c9r3 -t "IDEO"
```

### Gestión manual de comandos

Reset DB

```
docker-compose exec api /var/www/bin/directus install:database -f
```

Crear usuario

```
docker-compose exec api /var/www/bin/directus install:install -e "email" -p "password" -t "nombre instancia"
```